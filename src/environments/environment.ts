// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDy_2tgCCNkkVuSQbAOldvmQQTTNTKPByY',
    authDomain: 'crud-17b34.firebaseapp.com',
    databaseURL: 'https://crud-17b34.firebaseio.com',
    projectId: 'crud-17b34',
    storageBucket: 'crud-17b34.appspot.com',
    messagingSenderId: '233082740394'
  }

};
