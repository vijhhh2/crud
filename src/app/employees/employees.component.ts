import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from './shared/employee-service.service';



@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeServiceService]
})
export class EmployeesComponent implements OnInit {

  constructor(private empService: EmployeeServiceService) { }

  ngOnInit() {
  }

}
