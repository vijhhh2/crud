import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { EmployeeServiceService } from '../shared/employee-service.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private empService: EmployeeServiceService, private toastr: ToastrService) { }

  ngOnInit() {
    this.onRest();
  }

  onSubmit(employeeForm: NgForm) {
    if (employeeForm.value.$key == null) {
      this.empService.insertEmployee(employeeForm.value);
    this.toastr.success('SubmittedSucessfully', 'EmployeeRegister');
    } else {
      this.empService.updateEmployee(employeeForm.value);
      this.toastr.success('Updated successfully', 'Employee Register');
      this.empService.getData();
    }
    this.empService.editMode = false;
    this.onRest(employeeForm);
  }
  onRest(employeeForm?: NgForm) {
  if (employeeForm != null) {
    employeeForm.reset();
  }
  this.empService.selectedEmployee = {
    $key: null,
    name: '',
    position: '',
    office: '',
    salary: 0,
  };
  }
}
