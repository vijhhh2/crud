import { Component, OnInit } from '@angular/core';

import { EmployeeServiceService } from '../shared/employee-service.service';
import { Employee } from '../shared/employee.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employeeList: Employee[];

  constructor(private empService: EmployeeServiceService, private tostr: ToastrService) { }

  ngOnInit() {
   var x = this.empService.getData();
   x.snapshotChanges().subscribe(item => {
     this.employeeList = [];
     item.forEach((element) => {
      var y = element.payload.toJSON();
        y['$key'] = element.key;
      this.employeeList.push(y as Employee);
     });
   });
  }
  onEdit(emp: Employee) {
    this.empService.editMode = true;
    this.empService.selectedEmployee = Object.assign({}, emp);
  }
  onDelete(key: string) {
    if (confirm('Are you sure?') === true) {
      this.empService.deleteEmployee(key);
    }
    this.tostr.warning('Deleted Sucessfully', 'Employee register');
  }

}
